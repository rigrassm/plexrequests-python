init:
	pip install --upgrade pip
	pip install setuptools
	pip install -r requirements.txt

test:
	nosetests tests
