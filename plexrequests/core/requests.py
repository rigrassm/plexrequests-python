from search import Tvmaze
from db_models import Database tvshows, requests


class Requests(object):

	def add_request(self, request):
		# Add a new request
		try:
			with Database.transaction():
				tvshows.create(				
					title=request['title'],
					link=request['link'],
				    poster=request['poster'],
				    release=request['year'],
				    tvdbID=request['tvdb'],
				    summary=request['title'],
				    episodes=request['title']
					)

	# def update_request(self, req_id):
		# update already requested media

	# def delete_request(self, req_id):
		# Delete a request

	# def get_request(self, req_id):
		# Retrieve info about specific request

	# def list_requests(self, req_type="all"):
		# get list of requests by type, default returns all requests

