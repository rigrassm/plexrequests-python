import pytvmaze


class Tvmaze(object):

    def __init__(self, search_string=None, show_name=None, tvdb=None):
        self.search_string = search_string
        self.show_name = show_name
        self.tvdb = tvdb

    def search_shows(self, search):
        self.results = pytvmaze.get_show_list(search)
        return self.results

    def get_show(self, show_id):
        self.show = pytvmaze.get_show(show_id, embed='episodes')
        return self.show


# ## Show object attributes ##
# show.status
# show.rating
# show.genres
# show.weight
# show.updated
# show.name
# show.language
# show.schedule
# show.url
# show.image
# show.externals # dict of tvdb and tvrage id's if available
# show.premiered
# show.summary
# show.links # dict of previousepisode and nextepisode keys for their links
# show.web_channel
# show.runtime
# show.type
# show.id
# show.maze_id # same as show.id
# show.network # dict of network properties
# show.episodes # list of Episode objects
# show.seasons # dict of Season objects
#
# ## Season object attributes ##
# season.show
# season.episodes # dict of episodes
# season.id
# season.url
# season.season_number
# season.name
# season.episode_order
# season.premier_date
# season.end_date
# season.network
# season.web_channel
# season.image
# season.summary
# season.links
#
# ## Episode object attributes ##
# episode.title
# episode.airdate
# episode.url
# episode.season_number
# episode.episode_number
# episode.image
# episode.airstamp
# episode.runtime
# episode.maze_id
# episode.summary
