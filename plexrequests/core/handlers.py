import tornado.web
import search
import requests


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")

class SettingsHandler(BaseHandler):
    def get(self):
		self.render('index.html')

class SearchHandler(BaseHandler):
    def get(self):
		self.render('search.html')

    def post(self):
		search_string = self.get_argument('sstring')
		s = search.Tvmaze()
		if search_string == "":
			error = "Please enter a search term"
			self.render('results.html', r=error)
		else:
			results = s.search_shows(search_string)
			self.render('results.html', r=results)

class IndexHandler(BaseHandler):
    def get(self):
        self.render('index.html')

class RequestsHandler(BaseHandler):
	def get(self):
		self.render('requests.html')

	def post(self):
		show_id = self.get_argument('id')
		s = search.Tvmaze()
		info = s.get_show(show_id)
		print(info)
		

