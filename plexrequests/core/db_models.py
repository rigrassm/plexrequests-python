import logging
from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase
from plexrequests import config as conf
from passlib.hash import sha256_crypt

db_file = conf.config['paths']['db_path']
if db_file != False:
    db = SqliteExtDatabase(db_file)
else:
    logger.warn(db_file + ": Not a valid path")

class BaseModel(Model):
    class Meta:
        database = db

class plexuser(BaseModel):
    username = CharField(unique=True)
    password = CharField()
    email = CharField()
    role = CharField()

    def check_password(self, password):
        return sha256_crypt.verify(password, self.password)

    class Meta:
        order_by = ('username',)

class tvshows(BaseModel):
    id = IntegerField(unique=True)
    title = CharField()
    link = CharField()
    poster = CharField()
    release = DateField()
    tvdbID = IntegerField()
    summary = CharField()
    episodes = IntegerField

class movies(BaseModel):
    id = IntegerField(unique=True)
    title = CharField()
    link = CharField()
    poster = CharField()
    release = DateField()
    tmdbID = IntegerField()
    summary = CharField()

class requests(BaseModel):
    id = IntegerField(unique=True)
    type = CharField()
    fk_id = IntegerField()
    req_date = DateField()
    approved = IntegerField()
    available = IntegerField()

class Database(BaseModel):
    def db_dbect(self, db):
        self.db = db
        return db


def create_tables():
    db.connect()
    db.create_tables([plexuser, tvshows, movies, requests], True)
    db.commit()
    db.close()

create_tables()
