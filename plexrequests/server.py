import os
import logging
import tornado.ioloop
import tornado.web
from config import config as conf
import core
from core.handlers import IndexHandler, SearchHandler, SettingsHandler, RequestsHandler
from tornado.options import options, define, parse_command_line


class Server(object):

    def __init__(self):
        pass

    def check_config():
        # TODO Create logic to test if config values are sane
        pass

    def start(self):
        define('port', type=int, default=8000)
        define('url', type=str, default="")
        settings = {
            "template_path": conf['paths']['templates'],
            "cookie_secret": conf['keys']['tornado']
        }

        handlers = [
            (r"/", IndexHandler),
            (r"/settings", SettingsHandler),
            (r"/search", SearchHandler),
			(r"/requests", RequestsHandler)
		]

        parse_command_line()
        app = tornado.web.Application(handlers, **settings)

        app.listen(options.port)

        logger = logging.getLogger(__name__)
        logger.info('Server started at http://localhost:{}{}'.format(
          options.port, options.url))

        try:
            tornado.ioloop.IOLoop.instance().start()
        except KeyboardInterrupt:
            tornado.ioloop.IOLoop.instance().stop()
            logger.info('Server stopped')


if __name__ == '__main__':
    server = Server()
    server.start()
