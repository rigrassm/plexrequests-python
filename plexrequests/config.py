import os
from os.path import join as join

def base_path():
    return os.path.dirname(__file__)
config = {
    "paths": {
        'base_path': base_path(),
        'db_path': join(base_path(), 'core/db.sqlite'),
        'templates': join(base_path(), 'templates')
    },
    "keys": {
        'tornado': 'ggJMlvuGItsmA5n8iuk38iMSCtmDOnGG8iIjd8e',
        'tmdb': 'asdfa443r4e4r34f43f4feedsasdASDfa',
        'pushbullet': '',
        'plex': ''
    },
    "urls": {
        'plex_media_server': 'http://localhost:32400',
        'sonarr': 'http://localhost:8989',
        'couchpotato': 'http://localhost:5050',
        'sickrage': 'http://localhost:7878'
    }
}
